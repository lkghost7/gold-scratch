﻿using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace CodeBase.Extensions
{
    public static class GameExtension
    {
        public static void Shuffle<T>(this List<T> listShuffle)
        {
            for (int i = 0; i < listShuffle.Count; i++)
            {
                T temp = listShuffle[i];
                int randomIndex = Random.Range(i, listShuffle.Count);
                listShuffle[i] = listShuffle[randomIndex];
                listShuffle[randomIndex] = temp;
            }
        }
    }
}