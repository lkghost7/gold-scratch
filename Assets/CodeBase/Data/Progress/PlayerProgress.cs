using System;

namespace CodeBase.Data.Progress
{
    [Serializable]
    public class PlayerProgress
    {
        public float Balance;
        public PlayerProgress(float balance)
        {
            Balance = balance;
        }
    }
}