using UnityEngine;

namespace CodeBase.Data.Locations
{
    [CreateAssetMenu(fileName = "Position", menuName = "StaticData/PositionCards")]
    public class PositionCards : ScriptableObject
    {
        public CardCoordinates[] CardCoordinates;
    }
}