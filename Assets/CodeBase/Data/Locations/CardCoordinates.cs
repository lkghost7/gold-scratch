using System;

namespace CodeBase.Data.Locations
{
    [Serializable]
    public struct CardCoordinates
    {
        public float X;
        public float Y;
    }
}