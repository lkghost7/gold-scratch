using UnityEngine;

namespace CodeBase.Data.StaticData.ColorData
{
    [CreateAssetMenu(fileName = "ColorData", menuName = "StaticData/ColorData")]
    public class ColorData : ScriptableObject
    {
        public Color32 PositiveColor;
        public Color32 NegativeColor;
    }
}