using System;
using CodeBase.Data.Enums;
using UnityEngine;

namespace CodeBase.Data.StaticData.Sounds
{
    [Serializable]
    public class AudioClipData
    {
        public AudioClip Clip;
        public SoundId Id;
    } 
}