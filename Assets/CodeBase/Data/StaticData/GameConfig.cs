using UnityEngine;

namespace CodeBase.Data.StaticData
{
    [CreateAssetMenu(fileName = "GameConfig", menuName = "StaticData/GameConfig")]
    public class GameConfig : ScriptableObject
    {
        [Range(1, 100000)] public int Balance;
        [Range(1, 9)] public int LimitWinCard = 3;
        [Range(1, 100)] public int MaxCard = 9;
        [Range(1, 100)] public int BonusCount = 3;
        [Range(0, 1)] public float PercentClearCard;
        [Range(1, 10000)] public int TicketPrice;
        [Range(1, 1000000)] public int[] CardValues;
    }
}