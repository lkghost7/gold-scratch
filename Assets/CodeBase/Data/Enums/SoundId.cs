namespace CodeBase.Data.Enums
{
    public enum SoundId
    {
        ClearCard,
        ClickRefreshButton,
        WinSound,
        FailSound,
    }
}