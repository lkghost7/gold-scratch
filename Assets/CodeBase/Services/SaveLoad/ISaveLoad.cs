using CodeBase.Data.Progress;
using CodeBase.Infrastructure.ServiceContainer;

namespace CodeBase.Services.SaveLoad
{
    public interface ISaveLoad : IService
    {
        void SaveProgress();
        PlayerProgress LoadProgress();
    }
}