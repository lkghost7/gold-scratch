using System;
using CodeBase.Data.Progress;

namespace CodeBase.Services.PersistentProgress
{
    public class PersistentProgressService : IPersistentProgressService
    {
        public event Action<float> OnBalanceChanged;
        public PlayerProgress Progress { get; set; }

        public void AddBalance(float balance)
        {
            Progress.Balance += balance;
            OnBalanceChanged?.Invoke(Progress.Balance);
        }

        public void SubtractBalance(float balance)
        {
            Progress.Balance -= balance;
            OnBalanceChanged?.Invoke(Progress.Balance);
        }

        public void ResetBalance(float balance)
        {
            Progress.Balance = balance;
            OnBalanceChanged?.Invoke(Progress.Balance);
        }
    }
}