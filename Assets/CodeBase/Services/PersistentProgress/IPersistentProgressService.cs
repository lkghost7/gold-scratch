﻿using System;
using CodeBase.Data.Progress;
using CodeBase.Infrastructure.ServiceContainer;

namespace CodeBase.Services.PersistentProgress
{
    public interface IPersistentProgressService : IService
    {
        event Action<float> OnBalanceChanged;
        PlayerProgress Progress { get; set; }
        void AddBalance(float balance);
        void SubtractBalance(float balance);
        void ResetBalance(float balance);
    }
}