using CodeBase.Core.Cards;
using CodeBase.Core.Cards.View;
using CodeBase.Core.GamePlay;
using CodeBase.Core.UI.GamePlay;
using CodeBase.Data.Locations;
using CodeBase.Infrastructure.AssetManagement;
using CodeBase.Services.Factories.UIFactory;
using CodeBase.Services.StaticData;
using UnityEngine;

namespace CodeBase.Services.Factories.GameFactory
{
    public class GameFactory : IGameFactory
    {
        public WinIndicator[] WinIndicators { get; private set; }
        public ScratchTicket ScratchTicket { get; private set; }
        public BonusButton BonusButton { get; private set; }
        public CardScratch CardScratch { get; private set; }

        private readonly IAssetProvider _assets;
        private readonly IStaticDataService _staticDataService;

        public GameFactory(IAssetProvider assets, IStaticDataService staticDataService)
        {
            _assets = assets;
            _staticDataService = staticDataService;
        }

        public CardScratch[] CreateScratchCards()
        {
            PositionCards positionCards = _staticDataService.GetLocationCards();
            CardScratch[] scratchCardPresenters = new CardScratch[_staticDataService.GetGameConfig().MaxCard];
            for (var i = 0; i < positionCards.CardCoordinates.Length; i++)
            {
                CardCoordinates coordinate = positionCards.CardCoordinates[i];
                Vector2 position = new Vector2(coordinate.X, coordinate.Y);
                CardView cardView = CreateCardView(position);
                float percent = _staticDataService.GetGameConfig().PercentClearCard;
                CardScratch cardScratch = new CardScratch(cardView, i, percent, position);
                scratchCardPresenters[i] = cardScratch;
            }

            return scratchCardPresenters;
        }

        public ScratchTicket CreateScratchTicket()
        {
            ScratchTicket scratchTicket = new ScratchTicket(CreateScratchCards(), _staticDataService);
            ScratchTicket = scratchTicket;
            return ScratchTicket;
        }

        public CardView CreateCardView(Vector2 position)
        {
            GameObject card = _assets.Instantiate(path: AssetPath.CardPath, position);
            return card.GetComponent<CardView>();
        }

        public WinIndicator[] CreateWinIndicators()
        {
            int limitWinCard = _staticDataService.GetGameConfig().LimitWinCard;
            WinIndicator[] indicators = new WinIndicator[limitWinCard];
            for (int i = 0; i < limitWinCard; i++)
            {
                WinIndicator winIndicators = CreateWinIndicator();
                indicators[i] = winIndicators;
            }

            WinIndicators = indicators;
            return indicators;
        }

        public WinIndicator CreateWinIndicator()
        {
            GameObject card = _assets.Instantiate(path: AssetPath.IndicatorPath);
            return card.GetComponent<WinIndicator>();
        }

        public BonusButton CreateMonitorBonusButton(GameButton getBonusButton)
        {
            BonusButton bonus = new BonusButton(getBonusButton, _staticDataService.GetGameConfig().BonusCount);
            BonusButton = bonus;
            return BonusButton;
        }
    }
}