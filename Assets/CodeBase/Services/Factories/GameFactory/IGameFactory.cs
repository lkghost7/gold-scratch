using CodeBase.Core.Cards;
using CodeBase.Core.Cards.View;
using CodeBase.Core.GamePlay;
using CodeBase.Core.UI.GamePlay;
using CodeBase.Infrastructure.ServiceContainer;
using CodeBase.Services.Factories.UIFactory;
using UnityEngine;

namespace CodeBase.Services.Factories.GameFactory
{
    public interface IGameFactory : IService
    {
        ScratchTicket ScratchTicket { get; }
        BonusButton BonusButton { get; }
        CardScratch CardScratch { get; }
        CardScratch[] CreateScratchCards();
        ScratchTicket CreateScratchTicket();
        CardView CreateCardView(Vector2 position);
        WinIndicator[] CreateWinIndicators();
        WinIndicator[] WinIndicators { get; }
        WinIndicator CreateWinIndicator();
        BonusButton CreateMonitorBonusButton(GameButton getBonusButton);
    }
}