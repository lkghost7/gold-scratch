using CodeBase.Core.UI.GamePlay;
using CodeBase.Core.UI.Sound;
using CodeBase.Infrastructure.AssetManagement;
using CodeBase.Services.PersistentProgress;
using CodeBase.Services.SoundService;
using CodeBase.Services.StaticData;
using UnityEngine;

namespace CodeBase.Services.Factories.UIFactory
{
    public class UIFactory : IUIFactory
    {
        public GameButton RefreshButton { get; private set; }
        public BalanceTooltipView BalanceTooltipView { get; private set; }
        public GameButton ReturnButton { get; private set; }
        public GameButton GetBonusButton { get; private set; }
        public WinPopupView WinPopupView { get; private set; }
        public MainMenuPanel MainMenuPanel { get; private set; }
        public SoundSettingsView SoundSettingsView { get; private set; }
        public SoundSettings SoundSettings { get; private set; }
        public BalanceText Balance { get; private set; }
        
        private readonly IAssetProvider _asset;
        private readonly IPersistentProgressService _progress;
        private readonly ISoundService _soundService;
        private readonly IStaticDataService _staticData;
 
        public UIFactory(IAssetProvider asset, IPersistentProgressService progress,
            ISoundService soundService, IStaticDataService staticDataService)
        {
            _asset = asset;
            _progress = progress;
            _soundService = soundService;
            _staticData = staticDataService;
        }

        public GameObject CreateUiRoot() => _asset.Instantiate(AssetPath.UIRootPath);
        public BalanceText CreateBalanceText(Transform rootUI)
        {
            GameObject gameObject = _asset.Instantiate(AssetPath.UIBalanceTextPath, rootUI);
            BalanceText balanceText = gameObject.GetComponent<BalanceText>();
            balanceText.Construct(_progress); 
            Balance = balanceText;
            return Balance;
        }

        public SoundSettingsView CreateSoundSettingsButton(Transform rootUI)
        {
            GameObject gameObject = _asset.Instantiate(AssetPath.UISoundSettingsButtonPath, rootUI);
            SoundSettingsView soundSettings = gameObject.GetComponent<SoundSettingsView>();
            SoundSettingsView = soundSettings;
            SoundSettings = new SoundSettings(_soundService, SoundSettingsView);
            return SoundSettingsView;
        }
         
        public GameButton CreateRefreshButton(Transform rootUI)
        {
            GameObject gameObject = _asset.Instantiate(AssetPath.UIRefreshButton, rootUI);
            GameButton refreshButton = gameObject.GetComponent<GameButton>();
            RefreshButton = refreshButton;
            return RefreshButton;
        }

        public GameButton CreateGetBonusButton(Transform rootUI)
        {
            GameObject gameObject = _asset.Instantiate(AssetPath.UIGetBonusButton, rootUI);
            GameButton getBonusButton = gameObject.GetComponent<GameButton>();
            GetBonusButton = getBonusButton;
            return GetBonusButton;
        }

        public BalanceTooltipView CreateBalanceTooltipView(Transform rootUI)
        {
            GameObject gameObject = _asset.Instantiate(AssetPath.UIBalanceTooltip, rootUI);
            BalanceTooltipView balanceTooltipView = gameObject.GetComponent<BalanceTooltipView>();
            balanceTooltipView.Construct(_staticData);
            BalanceTooltipView = balanceTooltipView;
            return BalanceTooltipView;
        }

        public WinPopupView CreateWinPopupView(Transform rootUI)
        {
            GameObject gameObject = _asset.Instantiate(AssetPath.UIWinPopupView, rootUI);
            WinPopupView winPopupView = gameObject.GetComponent<WinPopupView>();
            WinPopupView = winPopupView;
            return WinPopupView;
        }

        public GameButton CreateReturnButton(Transform rootUI)
        {
            GameObject gameObject = _asset.Instantiate(AssetPath.UIReturnButton, rootUI);
            GameButton refreshButton = gameObject.GetComponent<GameButton>();
            ReturnButton = refreshButton;
            return ReturnButton;
        }
        
        public MainMenuPanel CreateMainMenuPanel(Transform rootUI)
        {
            GameObject gameObject = _asset.Instantiate(AssetPath.UISoundMainMenuPath, rootUI);
            MainMenuPanel mainMenuPanel = gameObject.GetComponent<MainMenuPanel>();
            MainMenuPanel = mainMenuPanel;
            return MainMenuPanel;
        }
    }
}