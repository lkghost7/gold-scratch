using CodeBase.Core.UI.GamePlay;
using CodeBase.Core.UI.Sound;
using CodeBase.Infrastructure.ServiceContainer;
using UnityEngine;

namespace CodeBase.Services.Factories.UIFactory
{
    public interface IUIFactory : IService
    {
        GameButton RefreshButton { get; }
        BalanceTooltipView BalanceTooltipView { get; }
        GameButton ReturnButton { get; }
        GameButton GetBonusButton { get; } 
        WinPopupView WinPopupView { get; }
        SoundSettingsView SoundSettingsView { get; }
        BalanceText Balance { get; }
        MainMenuPanel MainMenuPanel { get; }
        GameObject CreateUiRoot();
        BalanceText CreateBalanceText(Transform rootUI);
        SoundSettingsView CreateSoundSettingsButton(Transform rootUI);
        MainMenuPanel CreateMainMenuPanel(Transform rootUI);
        GameButton CreateReturnButton(Transform rootUI);
        GameButton CreateRefreshButton(Transform rootUI);
        GameButton CreateGetBonusButton(Transform rootUI);
        BalanceTooltipView CreateBalanceTooltipView(Transform rootUI);
        WinPopupView CreateWinPopupView(Transform rootUI);
    }
}