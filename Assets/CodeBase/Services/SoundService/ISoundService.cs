using CodeBase.Data.Enums;
using CodeBase.Data.StaticData.Sounds;
using CodeBase.Infrastructure.ServiceContainer;

namespace CodeBase.Services.SoundService
{
    public interface ISoundService : IService
    {
        void Construct(SoundData soundData);
        void EnableBackgroundMusic();
        void DisableBackgroundMusic();
        void PlayEffectSound(SoundId soundId);
        void SetBackgroundVolume(float volume);
        void SetEffectsVolume(float volume);
    } 
}