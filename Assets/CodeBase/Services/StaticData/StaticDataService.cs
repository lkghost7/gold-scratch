using CodeBase.Data.Locations;
using CodeBase.Data.StaticData;
using CodeBase.Data.StaticData.ColorData;
using CodeBase.Data.StaticData.Sounds;
using CodeBase.Infrastructure.AssetManagement;
using FortuneWheel.Scripts.Data;
using UnityEngine;

namespace CodeBase.Services.StaticData
{
    public class StaticDataService : IStaticDataService
    {
        public SoundData SoundData { get; set; }
        public ColorData ColorData { get; set; }
        public FortuneWheelPrefabs FortuneWheelData { get; set; }
        
        private GameConfig _gameConfig;
        private PositionCards _positionCardsConfig;

        public void LoadGameConfig()
        {
            _gameConfig = Resources.Load<GameConfig>(AssetPath.ConfigPath);
            SoundData = Resources.Load<SoundData>(AssetPath.SoundDataPath); 
            ColorData = Resources.Load<ColorData>(AssetPath.ColorDataPath); 
            FortuneWheelData = Resources.Load<FortuneWheelPrefabs>(AssetPath.FortuneWheelPrefabsPath);
        }
        
        public GameConfig GetGameConfig() => _gameConfig;
        public void LoadLocationCards() => _positionCardsConfig = Resources.Load<PositionCards>(AssetPath.PositionCardsPath);
        public PositionCards GetLocationCards() => _positionCardsConfig;
    }
}