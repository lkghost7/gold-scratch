using CodeBase.Data.Locations;
using CodeBase.Data.StaticData;
using CodeBase.Data.StaticData.ColorData;
using CodeBase.Data.StaticData.Sounds;
using CodeBase.Infrastructure.ServiceContainer;
using FortuneWheel.Scripts.Data;

namespace CodeBase.Services.StaticData
{
    public interface IStaticDataService : IService
    {
        void LoadGameConfig();
        GameConfig GetGameConfig();
        void LoadLocationCards();
        PositionCards GetLocationCards();
        SoundData SoundData { get; set; }
        ColorData ColorData { get; set; }
        FortuneWheelPrefabs FortuneWheelData { get; set; }
    }
}