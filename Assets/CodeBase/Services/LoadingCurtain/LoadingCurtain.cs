using System;
using DG.Tweening;
using UnityEngine;

namespace CodeBase.Services.LoadingCurtain
{
    public class LoadingCurtain : MonoBehaviour, ILoadingCurtain
    {
        public CanvasGroup _curtain;

        // public void Show()
        // {
        //     gameObject.SetActive(true);
        //     _curtain.DOFade(1, 1);
        // }
        public void Show(Action showIsDone)
        {
            gameObject.SetActive(true);
            _curtain.DOFade(1, 0.5f).OnComplete(() => { showIsDone?.Invoke(); });
        }

        // public void ShowOnComplete(Action showIsDone = null)
        // {
        //     gameObject.SetActive(true);
        //     _curtain.DOFade(1, 0.5f).OnComplete(() => { showIsDone?.Invoke(); });
        // }

 
        public void Hide()
        {
            _curtain
                .DOFade(0, 0.5f)
                .OnComplete(() => { gameObject.SetActive(false); });
        }

        private void Awake()
        {
            _curtain.alpha = 1f;
            DontDestroyOnLoad(this);
        }
    }
}