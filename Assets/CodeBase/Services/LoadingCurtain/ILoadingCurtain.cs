using System;
using CodeBase.Infrastructure.ServiceContainer;

namespace CodeBase.Services.LoadingCurtain
{
    public interface ILoadingCurtain : IService
    {
        void Show(Action onComplete);
        void Hide();
    }
}