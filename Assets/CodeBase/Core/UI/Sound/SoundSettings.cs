using CodeBase.Services.SoundService;

namespace CodeBase.Core.UI.Sound
{
    public class SoundSettings
    {
        private SoundSettingsView _soundSettingsView;
        private ISoundService _soundService;

        public SoundSettings(ISoundService soundService, SoundSettingsView soundSettingsView)
        {
            _soundSettingsView = soundSettingsView;
            _soundService = soundService;
            _soundSettingsView.OnSoundVolumeChanged += ChangeVolume;
        }

        private void ChangeVolume(float value)
        {
            _soundService.SetBackgroundVolume(value);
            _soundService.SetEffectsVolume(value);
        }

        ~SoundSettings() => _soundSettingsView.OnSoundVolumeChanged -= ChangeVolume;
    }
}