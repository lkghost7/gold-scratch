using System;
using UnityEngine;
using UnityEngine.UI;

namespace CodeBase.Core.UI.Sound
{
    public class SoundSettingsView : MonoBehaviour
    {
        public event Action<float> OnSoundVolumeChanged;

        [SerializeField] private Button button;
        [SerializeField] private Slider volumeSlider;

        private void Start()
        {
            button.onClick.AddListener(SwitchSoundSlider);
            volumeSlider.onValueChanged.AddListener(SendSoundVolumeChange);
        }

        private void OnDestroy()
        {
            button.onClick.RemoveListener(SwitchSoundSlider);
            volumeSlider.onValueChanged.RemoveListener(SendSoundVolumeChange);
        }

        private void SendSoundVolumeChange(float newVolume) => OnSoundVolumeChanged?.Invoke(newVolume);

        private void SwitchSoundSlider() => volumeSlider.gameObject.SetActive(!volumeSlider.gameObject.activeSelf);
    }
}