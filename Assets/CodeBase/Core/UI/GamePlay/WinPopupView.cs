﻿using DG.Tweening;
using TMPro;
using UnityEngine;

namespace CodeBase.Core.UI.GamePlay
{
    public class WinPopupView : MonoBehaviour
    {
        [SerializeField] private CanvasGroup _canvasGroup;
        [SerializeField] private TextMeshProUGUI _resultText;

        private const float FadeDuration = 0.75f;
 
        private void Awake()
        {
            _canvasGroup.alpha = 0f;
            gameObject.SetActive(false);
        }

        public void SetWinResultText(int result) =>
            _resultText.text = $"You win\n{result}";
        
        public void SetFailResultText() =>
            _resultText.text = $"You lose";
        
        public void Show()
        {
            gameObject.SetActive(true);
            _canvasGroup.DOFade(1, FadeDuration);
        }

        public void Hide()
        {
            _canvasGroup.DOFade(0, FadeDuration)
                .OnComplete(() => gameObject.SetActive(false));
        }
    }
}