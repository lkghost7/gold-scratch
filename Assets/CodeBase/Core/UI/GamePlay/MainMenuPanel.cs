using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace CodeBase.Core.UI.GamePlay
{
    public class MainMenuPanel : MonoBehaviour
    {
        public event Action OnPlayClick;

        [SerializeField] private Button _playButton;
        [SerializeField] private CanvasGroup _canvas;

        public void Show()
        {
            gameObject.SetActive(true);
            _canvas.alpha = 0;
            _canvas.DOFade(1, 0.5f);
        }

        public void Hide() => 
            _canvas.DOFade(0, 0.5f).OnComplete(() => { gameObject.SetActive(false); });

        public void DisableButton() => _playButton.interactable = false;

        private void Awake() => _playButton.onClick.AddListener(SendRefreshButtonClick);
        private void OnDestroy() => _playButton.onClick.RemoveAllListeners();
        private void SendRefreshButtonClick() => OnPlayClick?.Invoke();
    }
}