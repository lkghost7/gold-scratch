using CodeBase.Services.PersistentProgress;
using TMPro;
using UnityEngine;

namespace CodeBase.Core.UI.GamePlay
{
    public class BalanceText : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _balanceText;
        private IPersistentProgressService _progress;

        public void Construct(IPersistentProgressService progress)
        {
            _progress = progress;
            _balanceText.text = _progress.Progress.Balance.ToString();
            SubscribeBalance();
        }

        private void SetBalanceText(float balance) => _balanceText.text = balance.ToString();

        private void SubscribeBalance() =>
            _progress.OnBalanceChanged += SetBalanceText;

        private void OnDestroy() =>
            _progress.OnBalanceChanged -= SetBalanceText;
    }
}