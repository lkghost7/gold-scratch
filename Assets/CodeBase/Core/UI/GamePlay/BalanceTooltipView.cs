using CodeBase.Services.StaticData;
using DG.Tweening;
using TMPro;
using UnityEngine;

namespace CodeBase.Core.UI.GamePlay
{
    public class BalanceTooltipView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _text;
        [SerializeField] private CanvasGroup _canvas;

        private Color32 _positiveColor;
        private Color32 _negativeColor;
        private Vector2 _startPosition;
        private Tween _tween;

        public void Construct(IStaticDataService staticDataService)
        {
            _positiveColor = staticDataService.ColorData.PositiveColor;
            _negativeColor = staticDataService.ColorData.NegativeColor;
            _startPosition = transform.position;
        }

        public void SetText(int num)
        {
            if (num > 0)
            {
                SetPositiveValue(num);
            }
            else
            {
                SetNegativeValue(num);
            }
        }

        private void SetNegativeValue(int num)
        {
            _text.text = num.ToString();
            _text.color = _negativeColor;
        }

        private void SetPositiveValue(int num)
        {
            _text.text = "+" + num;
            _text.color = _positiveColor;
        }

        public void ShowBalanceTooltip()
        {
            _tween.Kill();
            _canvas.alpha = 0;
            _canvas.DOFade(1, 0.5f);
            transform.position = _startPosition;
            MoveTextUp();
        }

        private void MoveTextUp()
        {
            _tween = transform.DOMoveY(transform.position.y + 100, 2).SetDelay(0.5f);
            _canvas.DOFade(0, 1f).SetDelay(0.5f);
        }
    }
}