using System;
using UnityEngine;
using UnityEngine.UI;

namespace CodeBase.Core.UI.GamePlay
{
    public class GameButton : MonoBehaviour
    {
        public event Action OnButtonClick;
        [SerializeField] private Button _button;

        public  void Enable() => _button.interactable = true;
        public  void Disable() => _button.interactable = false;
        public void SetActiveOff() => gameObject.SetActive(false);
        public void SetActiveOn() => gameObject.SetActive(true);
        
        private void Awake() => _button.onClick.AddListener(SendButtonClick);
        private void OnDestroy() => _button.onClick.RemoveAllListeners();
        private  void SendButtonClick() => OnButtonClick?.Invoke();
    }
}