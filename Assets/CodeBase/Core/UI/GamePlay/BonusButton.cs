namespace CodeBase.Core.UI.GamePlay
{
    public class BonusButton
    {
        private readonly GameButton _getBonusButton;
        private readonly int _bonusCount;

        private int _bonusGameCount;

        public BonusButton(GameButton getBonusButton, int bonusCount)
        {
            _getBonusButton = getBonusButton;
            _bonusCount = bonusCount;
        }

        public void ResetBonusGameCount() => _bonusGameCount = 0;

        public void CheckBonusGame()
        {
            _bonusGameCount++;
            if (_bonusGameCount >= _bonusCount)
            {
                _bonusGameCount = 0;
                _getBonusButton.SetActiveOn();
            }
        }
    }
}