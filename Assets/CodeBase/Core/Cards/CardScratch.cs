﻿using CodeBase.Core.Cards.Model;
using CodeBase.Core.Cards.View;
using UnityEngine;

namespace CodeBase.Core.Cards
{
    public class CardScratch
    {
        public CardView CardView { get; }
        public CardModel CardModel { get; }

        public CardScratch(CardView cardView, int id, float limitPercent, Vector2 position)
        {
            CardView = cardView;
            CardView.InitViewCard(id, limitPercent);
            CardModel = new CardModel(id, position);
        }
 
        public void SetValueScratchCard(int value)
        {
            CardModel.SetValue(value);
            CardView.SetCardViewText(value);
        }
    }
}