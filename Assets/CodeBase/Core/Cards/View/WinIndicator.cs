using UnityEngine;

namespace CodeBase.Core.Cards.View
{
    public class WinIndicator : MonoBehaviour
    {
        [SerializeField] private GameObject indicator;

        public void ShowAndMovePosition(Vector2 position)
        {
            indicator.SetActive(true);
            transform.position = (position);
        }

        public void Hide() => indicator.SetActive(false);
    }
}