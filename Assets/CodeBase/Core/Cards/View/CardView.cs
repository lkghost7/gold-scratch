﻿using System;
using ScratchCard.Scripts;
using TMPro;
using UnityEngine;

namespace CodeBase.Core.Cards.View
{
    public class CardView : MonoBehaviour
    {
        public event Action<CardView> OnCompleteErase;

        [SerializeField] private TextMeshPro _cardValueText;
        [SerializeField] private EraseProgress _eraseProgress;
        [SerializeField] private ScratchCard.Scripts.ScratchCard _scratchCard;
 
        private int _idCardView;
        private float _percentLimit;

        public void InitViewCard(int id, float limitPercent)
        {
            _idCardView = id;
            _percentLimit = limitPercent;
        }

        public void SetCardViewText(int valueText) => _cardValueText.text = valueText.ToString();
        public void FillScratchCard() => _scratchCard.ClearInstantly();
        public void ClearScratchCard() => _scratchCard.FillInstantly();
        public void SubscribeToCheckCompleteErase() => _eraseProgress.OnProgress += CheckCompleteErase;
        
        private void OnDestroy() => _eraseProgress.OnProgress -= CheckCompleteErase;
        private void CheckCompleteErase(float progress)
        {
            if (!(_percentLimit <= progress)) return;
            OnCompleteErase?.Invoke(this);
            ClearScratchCard();
            _eraseProgress.OnProgress -= CheckCompleteErase;
        }
    }
}