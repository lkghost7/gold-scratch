﻿using UnityEngine;

namespace CodeBase.Core.Cards.Model
{
    public class CardModel
    {
        public int ID { get; }
        public int Value { get; private set; }
        private readonly Vector2 _position;

        public CardModel(int id, Vector2 position)
        {
            ID = id;
            _position = position;
        }

        public void SetValue(int value) => Value = value;
        public Vector2 GetPosition() => _position;
    }
}