﻿using System;
using System.Collections.Generic;
using CodeBase.Core.Cards;
using CodeBase.Core.Cards.View;
using CodeBase.Services.StaticData;
using Random = UnityEngine.Random;

namespace CodeBase.Core.GamePlay
{
    public class ScratchTicket
    { 
        public event Action<int> OnCompleteGame;
        public CardScratch[] ScratchCards { get; }

        private readonly Dictionary<int, List<CardScratch>> _scratchCardValueKeeper = new Dictionary<int, List<CardScratch>>();
        private readonly IStaticDataService _staticDataService;

        private int _winCardCounter;
        private int _winCardLimit;
        private int _winKey;

        public ScratchTicket(CardScratch[] scratchCards, IStaticDataService staticDataService)
        {
            ScratchCards = scratchCards;
            _staticDataService = staticDataService;
            InitScratchCardWinners();
        }
        
        public void ResetCards()
        {
            ResetValues();
            ClearScratchCardWinners();
            InitScratchCards();
            SubscribeScratchCards();
        }
        
        private void InitScratchCardWinners()
        {
            int[] cardValuesCount = _staticDataService.GetGameConfig().CardValues;

            foreach (int cardValue in cardValuesCount)
            {
                _scratchCardValueKeeper.Add(cardValue, new List<CardScratch>(_winCardLimit));
            }
        }

        private void ClearScratchCardWinners()
        {
            foreach (KeyValuePair<int, List<CardScratch>> pair in _scratchCardValueKeeper)
            {
                pair.Value.Clear();
            }
        }

        private void ResetValues()
        {
            _winCardLimit = _staticDataService.GetGameConfig().LimitWinCard;
            _winKey = 0;
            _winCardCounter = 0;
        }

        private int GetRandomCardValue()
        {
            int[] cardValuesCount = _staticDataService.GetGameConfig().CardValues;
            int randomNum = Random.Range(0, cardValuesCount.Length);
            return cardValuesCount[randomNum];
        }

        private void InitScratchCard(CardScratch scratchCard)
        {
            int cardValueKey = GetRandomCardValue();

            while (_scratchCardValueKeeper[cardValueKey].Count >= _winCardLimit)
            {
                cardValueKey = GetRandomCardValue();
            }

            scratchCard.SetValueScratchCard(cardValueKey);
            _scratchCardValueKeeper[cardValueKey].Add(scratchCard);

            if (_winKey > 0) return;

            if (_scratchCardValueKeeper[cardValueKey].Count == _winCardLimit)
            {
                _winKey = cardValueKey;
                _winCardLimit--;
            }
        }

        private void InitScratchCards()
        {
            foreach (CardScratch cardScratch in ScratchCards)
            {
                InitScratchCard(cardScratch);
            }
        }

        private void SubscribeScratchCards()
        {
            if (_winKey != 0)
            {
                SubscribeWinCards();
            }
            else
            {
                SubscribeGameOverCards();
            }
        }

        private void SubscribeGameOverCards()
        {
            foreach (CardScratch scratchCard in ScratchCards)
            {
                scratchCard.CardView.SubscribeToCheckCompleteErase();
                scratchCard.CardView.OnCompleteErase += CheckEndGame;
            }
        }

        private void SubscribeWinCards()
        {
            List<CardScratch> scratchCards = _scratchCardValueKeeper[_winKey];
            foreach (CardScratch cardScratch in scratchCards)
            {
                cardScratch.CardView.SubscribeToCheckCompleteErase();
                cardScratch.CardView.OnCompleteErase += CheckEndGame;
            }
        } 
 
        private void CheckEndGame(CardView cardView)
        {
            cardView.OnCompleteErase -= CheckEndGame;
            _winCardCounter++;

            if (_winKey != 0)
            {
                CheckWinGame();
            }
            else
            {
                CheckGameOver();
            }
        }

        private void CheckWinGame()
        {
            if (_winCardCounter < _staticDataService.GetGameConfig().LimitWinCard) return;
            OnCompleteGame?.Invoke(_winKey);
        }
 
        private void CheckGameOver()
        {
            if (_winCardCounter < ScratchCards.Length) return;
            OnCompleteGame?.Invoke(0);
        }
    }
}