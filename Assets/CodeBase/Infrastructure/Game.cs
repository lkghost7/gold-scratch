using CodeBase.Infrastructure.ServiceContainer;
using CodeBase.Infrastructure.States;
using CodeBase.Services.LoadingCurtain;
using CodeBase.Services.SceneLoader;
using CodeBase.Services.SoundService;

namespace CodeBase.Infrastructure
{
    public class Game
    {
        public readonly GameStateMachine StateMachine;

        public Game(ICoroutineRunner coroutineRunner, LoadingCurtain curtain, ISoundService soundService)
        {
            StateMachine = new GameStateMachine(new SceneLoader(coroutineRunner), curtain, AllServices.Container, soundService);
        }
    }
}