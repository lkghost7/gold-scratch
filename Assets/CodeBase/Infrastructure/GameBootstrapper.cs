﻿using CodeBase.Infrastructure.States;
using CodeBase.Services.LoadingCurtain;
using CodeBase.Services.SoundService;
using UnityEngine;

namespace CodeBase.Infrastructure
{
  public class GameBootstrapper : MonoBehaviour, ICoroutineRunner
  {
    public LoadingCurtain curtain;
    private Game _game;
    [SerializeField] private SoundService _soundService;

    private void Awake()
    {
      _game = new Game(this, curtain, _soundService);
      _game.StateMachine.Enter<BootstrapState>();

      DontDestroyOnLoad(this);
    }
  }
}