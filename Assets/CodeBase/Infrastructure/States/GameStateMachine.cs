﻿using System;
using System.Collections.Generic;
using CodeBase.Infrastructure.ServiceContainer;
using CodeBase.Services.Factories.GameFactory;
using CodeBase.Services.Factories.UIFactory;
using CodeBase.Services.LoadingCurtain;
using CodeBase.Services.PersistentProgress;
using CodeBase.Services.SaveLoad;
using CodeBase.Services.SceneLoader;
using CodeBase.Services.SoundService;
using CodeBase.Services.StaticData;
using FortuneWheel.Scripts.Data;
using FortuneWheel.Scripts.Infrastructure.Factory;

namespace CodeBase.Infrastructure.States
{
    public class GameStateMachine
    {
        private readonly Dictionary<Type, IExitableState> _states;
        private IExitableState _activeState;
        private FortuneWheelPrefabs data;

        public GameStateMachine(SceneLoader sceneLoader, LoadingCurtain loadingCurtain, AllServices services, ISoundService soundService)
        {
            _states = new Dictionary<Type, IExitableState>
            { 
                [typeof(BootstrapState)] = new BootstrapState(this, sceneLoader, services, soundService),
                [typeof(MainMenuState)] = new MainMenuState(this,  sceneLoader, loadingCurtain, services.Single<IUIFactory>()),
                [typeof(FortuneWheelState)] = new FortuneWheelState(this,  sceneLoader, loadingCurtain, new FortuneWheelFactory(services.Single<IStaticDataService>()), services.Single<IPersistentProgressService>() ,services.Single<ISaveLoad>()),
                [typeof(LoadLevelState)] = new LoadLevelState(this, sceneLoader, loadingCurtain, services.Single<IGameFactory>(), services.Single<IUIFactory>(), services.Single<IStaticDataService>(), services.Single<IPersistentProgressService>(), services.Single<ISoundService>(), services.Single<ISaveLoad>()),
                [typeof(PrepareGameplayState)] = new PrepareGameplayState(this, services.Single<IGameFactory>()),
                [typeof(ScratchCardState)] = new ScratchCardState(this, services.Single<IGameFactory>(),services.Single<IPersistentProgressService>(), soundService, services.Single<IUIFactory>(), loadingCurtain, services.Single<IStaticDataService>()),
                [typeof(CalculateWinState)] = new CalculateWinState(this, services.Single<IGameFactory>(), services.Single<IStaticDataService>(), services.Single<IPersistentProgressService>(), services.Single<IUIFactory>(), soundService, services.Single<ISaveLoad>(), loadingCurtain)
            }; 
        }
 
        public void Enter<TState>() where TState : class, IState
        {
            IState state = ChangeState<TState>();
            state.Enter();
        }

        public void Enter<TState, TPayload>(TPayload payload) where TState : class, IPayloadedState<TPayload>
        {
            TState state = ChangeState<TState>();
            state.Enter(payload);
        }
        
        private TState ChangeState<TState>() where TState : class, IExitableState
        {
            _activeState?.Exit();

            TState state = GetState<TState>();
            _activeState = state;

            return state;
        }

        private TState GetState<TState>() where TState : class, IExitableState =>
            _states[typeof(TState)] as TState;
    }
}