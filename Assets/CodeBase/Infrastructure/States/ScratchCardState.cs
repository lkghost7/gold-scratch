﻿using CodeBase.Core.Cards;
using CodeBase.Data.Enums;
using CodeBase.Infrastructure.AssetManagement;
using CodeBase.Services.Factories.GameFactory;
using CodeBase.Services.Factories.UIFactory;
using CodeBase.Services.LoadingCurtain;
using CodeBase.Services.PersistentProgress;
using CodeBase.Services.SoundService;
using CodeBase.Services.StaticData;
using UnityEngine;

namespace CodeBase.Infrastructure.States
{
    public class ScratchCardState : IState
    {
        private readonly GameStateMachine _stateMachine;
        private readonly IGameFactory _gameFactory;
        private readonly IPersistentProgressService _progress;
        private readonly ISoundService _soundSoundService;
        private readonly IUIFactory _uiFactory;
        private readonly LoadingCurtain _loadingCurtain;
        private readonly IStaticDataService _staticDataService;

        public ScratchCardState(GameStateMachine stateMachine, IGameFactory gameFactory,
            IPersistentProgressService progress, ISoundService soundService,
            IUIFactory uiFactory, LoadingCurtain loadingCurtain, IStaticDataService staticDataService)
        {
            _stateMachine = stateMachine;
            _gameFactory = gameFactory;
            _progress = progress;
            _soundSoundService = soundService;
            _uiFactory = uiFactory;
            _loadingCurtain = loadingCurtain;
            _staticDataService = staticDataService;
        }

        public void Exit()
        {
            _gameFactory.ScratchTicket.OnCompleteGame -= EndGame;
            _uiFactory.ReturnButton.OnButtonClick -= PressReturnReturnButton;
            _uiFactory.GetBonusButton.OnButtonClick -= PressGetBonusButton;
        }

        public void Enter()
        {
            _gameFactory.ScratchTicket.OnCompleteGame += EndGame;
            _uiFactory.ReturnButton.OnButtonClick += PressReturnReturnButton;
            _uiFactory.GetBonusButton.OnButtonClick += PressGetBonusButton;
        }
         
        private void PressGetBonusButton()
        {
            _loadingCurtain.Show(EnterFortuneWheelState);
            _uiFactory.GetBonusButton.Disable();
        }
        
        private void EnterFortuneWheelState() => _stateMachine.Enter<FortuneWheelState, string>(AssetPath.FortuneWheel);

        private void PressReturnReturnButton()
        {
            _loadingCurtain.Show(EnterMainMenuState);
            _uiFactory.ReturnButton.Disable();
        }

        private void EndGame(int winNum)
        {
            _progress.AddBalance(winNum);
            ShowWinIndicators(winNum);
            PlayEndRoundSound(winNum);
            CheckWinResult(winNum);
            _stateMachine.Enter<CalculateWinState>();
        }

        private void CheckWinResult(int winNum)
        {
            if (winNum == 0)
            {
                _uiFactory.WinPopupView.SetFailResultText();
            }
            else
            {
                _uiFactory.WinPopupView.SetWinResultText(winNum);
                _uiFactory.BalanceTooltipView.SetText(winNum);
                _uiFactory.BalanceTooltipView.ShowBalanceTooltip();
            }

            _uiFactory.WinPopupView.Show();
        }
        
        private void EnterMainMenuState() => _stateMachine.Enter<MainMenuState, string>(AssetPath.MainMenu);
        private void PlayEndRoundSound(int winNum) =>
            _soundSoundService.PlayEffectSound(winNum != 0 ? SoundId.WinSound : SoundId.FailSound);

        private void ShowWinIndicators(int winNum)
        {
            if (winNum == 0)
                return;

            CardScratch[] cards = _gameFactory.ScratchTicket.ScratchCards;
            int counter = 0;

            foreach (var cardScratch in cards)
            {
                if (cardScratch.CardModel.Value == winNum)
                {
                    Vector2 position = cardScratch.CardModel.GetPosition();
                    _gameFactory.WinIndicators[counter].ShowAndMovePosition(position);
                    counter++;
                }
            }
        }
    }
}