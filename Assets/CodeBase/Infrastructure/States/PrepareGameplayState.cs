using CodeBase.Core.Cards.View;
using CodeBase.Services.Factories.GameFactory;

namespace CodeBase.Infrastructure.States
{
    public class PrepareGameplayState : IState
    {
        private readonly GameStateMachine _stateMachine;
        private readonly IGameFactory _gameFactory;

        public PrepareGameplayState(GameStateMachine stateMachine, IGameFactory gameFactory)
        {
            _stateMachine = stateMachine;
            _gameFactory = gameFactory;
        }
 
        public void Exit() {}

        public void Enter()
        {
            HideWinIndicators();
            _gameFactory.ScratchTicket.ResetCards();
            _stateMachine.Enter<ScratchCardState>();
        }

        private void HideWinIndicators()
        {
            foreach (WinIndicator winIndicator in _gameFactory.WinIndicators)
            {
                winIndicator.Hide();
            }
        }
    }
}