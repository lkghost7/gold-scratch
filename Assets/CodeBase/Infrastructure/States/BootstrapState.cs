﻿using CodeBase.Infrastructure.AssetManagement;
using CodeBase.Infrastructure.ServiceContainer;
using CodeBase.Services.Factories.GameFactory;
using CodeBase.Services.Factories.UIFactory;
using CodeBase.Services.PersistentProgress;
using CodeBase.Services.SaveLoad;
using CodeBase.Services.SceneLoader;
using CodeBase.Services.SoundService;
using CodeBase.Services.StaticData;

namespace CodeBase.Infrastructure.States
{
    public class BootstrapState : IState
    {
        private readonly GameStateMachine _stateMachine;
        private readonly SceneLoader _sceneLoader;
        private readonly AllServices _services;

        public BootstrapState(GameStateMachine stateMachine, SceneLoader sceneLoader, 
            AllServices services, ISoundService soundService)
        {
            _stateMachine = stateMachine;
            _sceneLoader = sceneLoader;
            _services = services;

            RegisterServices(soundService);
        }

        public void Enter()
        {
            _sceneLoader.Load(AssetPath.InitialScene, onLoaded: EnterMainMenu);
        }

        public void Exit()
        {
        }

        private void RegisterServices(ISoundService soundService)
        {
            RegisterBaseService();
            RegisterStaticData();
            RegisterSoundService(soundService);
            RegisterUiFactory(soundService);
            RegisterFactory();
        }
         
        private void RegisterSoundService(ISoundService soundService)
        {
            soundService.Construct(_services.Single<IStaticDataService>().SoundData);
            _services.RegisterSingle<ISoundService>(soundService);
        }

        private void RegisterBaseService()
        {
            _services.RegisterSingle<IAssetProvider>(new AssetProvider());
            _services.RegisterSingle<IStaticDataService>(new StaticDataService());
            _services.RegisterSingle<IPersistentProgressService>(new PersistentProgressService());
            _services.RegisterSingle<ISaveLoad>(new PrefsSaveLoad(_services.Single<IPersistentProgressService>()));
        }

        private void RegisterFactory()
        {
            _services.RegisterSingle<IGameFactory>(new GameFactory(
                _services.Single<IAssetProvider>(), _services.Single<IStaticDataService>()));
        }

        private void RegisterUiFactory(ISoundService soundService)
        {
            _services.RegisterSingle<IUIFactory>(new UIFactory(
                _services.Single<IAssetProvider>(),
                _services.Single<IPersistentProgressService>(), soundService,
                _services.Single<IStaticDataService>()));
        }

        private void RegisterStaticData()
        {
            IStaticDataService staticData = new StaticDataService();
            staticData.LoadGameConfig();
            staticData.LoadLocationCards();
            _services.RegisterSingle(staticData);
        }
        
        private void EnterMainMenu() => _stateMachine.Enter<MainMenuState, string>(AssetPath.MainMenu);
    }
}