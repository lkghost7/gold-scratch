using CodeBase.Services.Factories.UIFactory;
using CodeBase.Services.LoadingCurtain;
using CodeBase.Services.SceneLoader;
using UnityEngine;

namespace CodeBase.Infrastructure.States
{
    public class MainMenuState : IPayloadedState<string>
    {
        private readonly GameStateMachine _stateMachine;
        private readonly SceneLoader _sceneLoader;
        private readonly IUIFactory _uiFactory;
        private readonly LoadingCurtain _loadingCurtain;
 
        public MainMenuState(GameStateMachine gameStateMachine, SceneLoader sceneLoader,
            LoadingCurtain loadingCurtain, IUIFactory uiFactory)
        {
            _stateMachine = gameStateMachine;
            _sceneLoader = sceneLoader;
            _loadingCurtain = loadingCurtain;
            _uiFactory = uiFactory;
        }
        
        public void Enter(string loadScene) =>
            _sceneLoader.Load(loadScene, OnLoaded);

        public void Exit() => _uiFactory.MainMenuPanel.OnPlayClick -= PressPlayButton;

        private void OnLoaded()
        {
            _loadingCurtain.Hide();
            GameObject rootUi = _uiFactory.CreateUiRoot();
            _uiFactory.CreateMainMenuPanel(rootUi.transform);
            _uiFactory.MainMenuPanel.OnPlayClick += PressPlayButton;
        }

        private void PressPlayButton()
        {
            _loadingCurtain.Show(EnterLoadLevel);
            _uiFactory.MainMenuPanel.DisableButton();
        }

        private void EnterLoadLevel() =>
            _stateMachine.Enter<LoadLevelState, int>(0);
    }
}