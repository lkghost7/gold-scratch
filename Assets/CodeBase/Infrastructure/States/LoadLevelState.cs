﻿using CodeBase.Data.Progress;
using CodeBase.Infrastructure.AssetManagement;
using CodeBase.Services.Factories.GameFactory;
using CodeBase.Services.Factories.UIFactory;
using CodeBase.Services.LoadingCurtain;
using CodeBase.Services.PersistentProgress;
using CodeBase.Services.SaveLoad;
using CodeBase.Services.SceneLoader;
using CodeBase.Services.SoundService;
using CodeBase.Services.StaticData;
using UnityEngine;

namespace CodeBase.Infrastructure.States
{
    public class LoadLevelState : IPayloadedState<int>
    {
        private readonly GameStateMachine _stateMachine;
        private readonly SceneLoader _sceneLoader;
        private readonly LoadingCurtain _loadingCurtain;
        private readonly IGameFactory _gameFactory;
        private readonly IUIFactory _uiFactory;
        private readonly IStaticDataService _staticDataService;
        private readonly IPersistentProgressService _progress;
        private readonly ISoundService _soundService;
        private readonly ISaveLoad _saveLoad;
        private int _bonusWheelResult;

        public LoadLevelState(GameStateMachine gameStateMachine, SceneLoader sceneLoader, LoadingCurtain loadingCurtain,
            IGameFactory gameFactory, IUIFactory uiFactory, IStaticDataService staticDataService,
            IPersistentProgressService progress, ISoundService soundService, ISaveLoad saveLoad)
        {
            _stateMachine = gameStateMachine;
            _sceneLoader = sceneLoader;
            _loadingCurtain = loadingCurtain;
            _gameFactory = gameFactory;
            _uiFactory = uiFactory;
            _staticDataService = staticDataService;
            _progress = progress;
            _soundService = soundService;
            _saveLoad = saveLoad;
        }

        public void Enter(int value)
        {
            _bonusWheelResult = value;
            _sceneLoader.Load(AssetPath.MainScene, OnLoaded);
        }

        public void Exit()
        {
        }
     
        private void OnLoaded()
        { 
            LoadProgressOrInitNew(); 
            CreateUI();
            CreateGamePlayComponents();
            _soundService.EnableBackgroundMusic();
            _stateMachine.Enter<PrepareGameplayState>();
            ShowTooltipFromFortuneWheel();
            _loadingCurtain.Hide();
        }

        private void ShowTooltipFromFortuneWheel()
        {
            if (_bonusWheelResult == 0)
                return;
            
            _uiFactory.BalanceTooltipView.SetText(_bonusWheelResult);
            _uiFactory.BalanceTooltipView.ShowBalanceTooltip();
            _bonusWheelResult = 0;
        }

        private Transform CreateUI()
        { 
            Transform uiRoot = _uiFactory.CreateUiRoot().transform;
            _uiFactory.CreateBalanceText(uiRoot);
            _uiFactory.CreateRefreshButton(uiRoot);
            _uiFactory.CreateReturnButton(uiRoot);
            _uiFactory.CreateGetBonusButton(uiRoot);
            _uiFactory.CreateBalanceTooltipView(uiRoot);
            _uiFactory.CreateWinPopupView(uiRoot);
            _uiFactory.CreateSoundSettingsButton(uiRoot);
            return uiRoot;
        }
        
        private void LoadProgressOrInitNew() => _progress.Progress = _saveLoad.LoadProgress() ?? CreateNewProgress();

        private void CreateGamePlayComponents()
        {
            _gameFactory.CreateScratchTicket();
            _gameFactory.CreateWinIndicators();
            _gameFactory.CreateMonitorBonusButton(_uiFactory.GetBonusButton);
        }
        
        private PlayerProgress CreateNewProgress()
        {
            PlayerProgress playerProgress = new PlayerProgress(_staticDataService.GetGameConfig().Balance);
            _progress.Progress = playerProgress;
            return playerProgress;
        }
    }
}