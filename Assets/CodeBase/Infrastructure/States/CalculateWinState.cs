using CodeBase.Core.Cards;
using CodeBase.Data.Enums;
using CodeBase.Infrastructure.AssetManagement;
using CodeBase.Services.Factories.GameFactory;
using CodeBase.Services.Factories.UIFactory;
using CodeBase.Services.LoadingCurtain;
using CodeBase.Services.PersistentProgress;
using CodeBase.Services.SaveLoad;
using CodeBase.Services.SoundService;
using CodeBase.Services.StaticData;

namespace CodeBase.Infrastructure.States
{
    public class CalculateWinState : IState
    {
        private readonly GameStateMachine _stateMachine;
        private readonly IGameFactory _gameFactory;
        private readonly IPersistentProgressService _progress;
        private readonly IStaticDataService _staticDataService;
        private readonly IUIFactory _uiFactory;
        private readonly ISoundService _soundService;
        private readonly ISaveLoad _saveLoad;
        private readonly LoadingCurtain _loadingCurtain;

        public CalculateWinState(GameStateMachine stateMachine, IGameFactory gameFactory,
            IStaticDataService staticDataService,
            IPersistentProgressService progress, IUIFactory uiFactory, ISoundService soundService,
            ISaveLoad saveLoad, LoadingCurtain loadingCurtain)
        {
            _stateMachine = stateMachine;
            _gameFactory = gameFactory;
            _staticDataService = staticDataService;
            _progress = progress;
            _uiFactory = uiFactory;
            _soundService = soundService;
            _saveLoad = saveLoad;
            _loadingCurtain = loadingCurtain;
        }

        public void Exit()
        {
            FillScratchCards();
            _uiFactory.RefreshButton.OnButtonClick -= PressRefreshButton;
            _uiFactory.GetBonusButton.OnButtonClick -= PressGetBonusButton;
            _uiFactory.ReturnButton.OnButtonClick -= PressReturnReturnButton;
            _uiFactory.RefreshButton.Disable();
        }

        public void Enter()
        {
            _uiFactory.RefreshButton.OnButtonClick += PressRefreshButton;
            _uiFactory.GetBonusButton.OnButtonClick += PressGetBonusButton;
            _uiFactory.ReturnButton.OnButtonClick += PressReturnReturnButton;
            _uiFactory.RefreshButton.Enable();
            ClearScratchTicket();
            _gameFactory.BonusButton.CheckBonusGame();
        }

        private void PressReturnReturnButton()
        {
            _loadingCurtain.Show(EnterMainMenuState);
            _gameFactory.BonusButton.ResetBonusGameCount();
            DisableButtons();
        }

        private void PressGetBonusButton()
        {
            _loadingCurtain.Show(EnterFortuneWheelState);
            _gameFactory.BonusButton.ResetBonusGameCount();
            DisableButtons();
        }

        private void DisableButtons()
        {
            _uiFactory.GetBonusButton.Disable();
            _uiFactory.RefreshButton.Disable();
            _uiFactory.ReturnButton.Disable();
        }

        private void EnterMainMenuState() => _stateMachine.Enter<MainMenuState, string>(AssetPath.MainMenu);
        private void EnterFortuneWheelState() => _stateMachine.Enter<FortuneWheelState, string>(AssetPath.FortuneWheel);

        private void PressRefreshButton()
        {
            _uiFactory.WinPopupView.Hide();
            _soundService.PlayEffectSound(SoundId.ClickRefreshButton);
            _uiFactory.BalanceTooltipView.SetText(-_staticDataService.GetGameConfig().TicketPrice);
            _uiFactory.BalanceTooltipView.ShowBalanceTooltip();
            CalculateBalance();
            _saveLoad.SaveProgress();
            _stateMachine.Enter<PrepareGameplayState>();
        }

        private void CalculateBalance()
        {
            _progress.SubtractBalance(_staticDataService.GetGameConfig().TicketPrice);

            if (_progress.Progress.Balance <= 0)
            {
                _progress.ResetBalance(_staticDataService.GetGameConfig().Balance);
            }
        }

        private void FillScratchCards()
        {
            CardScratch[] cards = _gameFactory.ScratchTicket.ScratchCards;
            foreach (CardScratch cardPresenter in cards)
            {
                cardPresenter.CardView.FillScratchCard();
            }
        }

        private void ClearScratchTicket()
        {
            CardScratch[] cards = _gameFactory.ScratchTicket.ScratchCards;
            foreach (CardScratch cardPresenter in cards)
            {
                cardPresenter.CardView.ClearScratchCard();
            }
        }
    }
}