using CodeBase.Services.LoadingCurtain;
using CodeBase.Services.PersistentProgress;
using CodeBase.Services.SaveLoad;
using CodeBase.Services.SceneLoader;
using FortuneWheel.Scripts.Infrastructure.Factory;
using EasyUI.PickerWheelUI;
using FortuneWheel.Scripts.Core.PickerWheel;
using FortuneWheel.Scripts.Core.UI;
using UnityEngine;

namespace CodeBase.Infrastructure.States
{
    public class FortuneWheelState : IPayloadedState<string>
    {
        private readonly GameStateMachine _stateMachine;
        private readonly SceneLoader _sceneLoader;
        private readonly LoadingCurtain _loadingCurtain;
        private readonly IFortuneWheelFactory _factory;
        private readonly IPersistentProgressService _progress;
        private readonly ISaveLoad _saveLoad;

        private PickerWheel _pickerWheel;
        private SpinButton _spinButton;
        private TakeBonusButton _takeBonusButton;
        private ResultPopupView _resultPopup;
        private int _bonusWheelResult;

        public FortuneWheelState(GameStateMachine gameStateMachine, SceneLoader sceneLoader,
            LoadingCurtain loadingCurtain, FortuneWheelFactory fortuneWheelFactory,
            IPersistentProgressService progress, ISaveLoad saveLoad)
        {
            _stateMachine = gameStateMachine;
            _sceneLoader = sceneLoader;
            _loadingCurtain = loadingCurtain;
            _factory = fortuneWheelFactory;
            _progress = progress;
            _saveLoad = saveLoad;
        }

        public void Enter(string loadScene) => _sceneLoader.Load(loadScene, OnLoaded);
        public void Exit() => _spinButton.OnSpinClicked -= Spin;

        private void CreateGame()
        {
            Transform rootUI = _factory.CreateRootUI().transform;
            _pickerWheel = _factory.CreateFortuneWheel(rootUI);
            _spinButton = _factory.CreateSpinButton(rootUI);
            _takeBonusButton = _factory.CreateReturnGameButton(rootUI);
            _resultPopup = _factory.CreateResultPopup(rootUI);

            _spinButton.OnSpinClicked += Spin;
            _takeBonusButton.OnReturnClicked += EndGameFortuneWheel;
            _pickerWheel.OnSpinEnd(FinishSpin);
        }

        private void Spin()
        {
            _pickerWheel.Spin(TakeButtonOn);
            _spinButton.Disable();
        }

        private void TakeButtonOn() => _takeBonusButton.SetActiveOn();

        private void EndGameFortuneWheel()
        {
            _loadingCurtain.Show(EnterLoadLevel);
            _takeBonusButton.OnReturnClicked -= EndGameFortuneWheel;
        }

        private void EnterLoadLevel() => _stateMachine.Enter<LoadLevelState, int>(_bonusWheelResult);

        private void FinishSpin(WheelPiece result)
        {
            _bonusWheelResult = result.Amount;
            _resultPopup.SetResultText(result.Amount);
            _progress.AddBalance(result.Amount);
            _saveLoad.SaveProgress();
            _resultPopup.Show();
        }

        private void OnLoaded()
        {
            _loadingCurtain.Hide();
            CreateGame();
        }
    }
}