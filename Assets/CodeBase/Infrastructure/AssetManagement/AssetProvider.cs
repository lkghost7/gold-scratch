using UnityEngine;
 
namespace CodeBase.Infrastructure.AssetManagement
{
    public class AssetProvider : IAssetProvider 
    {
        public GameObject Instantiate(string path, Transform target) 
        {
            var prefab = Resources.Load<GameObject>(path);
            return Object.Instantiate(prefab, target);
        }

        public GameObject Instantiate(string path)
        {
            var prefab = Resources.Load<GameObject>(path);
            return Object.Instantiate(prefab);
        }

        public GameObject Instantiate(string path, Vector2 position)
        {
            var prefab = Resources.Load<GameObject>(path);
            return Object.Instantiate(prefab, position, Quaternion.identity);
        }
    }
}