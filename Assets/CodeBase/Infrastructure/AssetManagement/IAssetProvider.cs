using CodeBase.Infrastructure.ServiceContainer;
using UnityEngine;

namespace CodeBase.Infrastructure.AssetManagement
{
    public interface IAssetProvider : IService
    {
        GameObject Instantiate(string path, Transform transform);
        GameObject Instantiate(string path);
        GameObject Instantiate(string path, Vector2 position);
    }
}