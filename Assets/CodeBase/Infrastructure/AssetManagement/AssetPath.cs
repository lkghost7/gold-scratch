namespace CodeBase.Infrastructure.AssetManagement
{
    public static class AssetPath
    {
        public const string InitialScene = "Initial";
        public const string MainScene = "Main";
        public const string MainMenu = "MainMenu";
        public const string FortuneWheel = "FortuneWheel";
        
        public const string CardPath = "Prefabs/Cards/CardView";
        public const string IndicatorPath = "Prefabs/Indicators/Indicator";

        public const string UIRootPath = "Prefabs/Ui/UiRoot";
        public const string UISoundSettingsButtonPath = "Prefabs/Ui/Buttons/SoundSettingsButton";
        public const string UISoundMainMenuPath = "Prefabs/Ui/Panels/MainMenuPanel";
        public const string UIRefreshButton = "Prefabs/Ui/Buttons/RefreshButton";
        public const string UIReturnButton = "Prefabs/Ui/Buttons/ReturnButton";
        public const string UIGetBonusButton = "Prefabs/Ui/Buttons/GetBonusButton";
        public const string UIBalanceTooltip = "Prefabs/Ui/Balance/BalanceTooltip";
        public const string UIWinPopupView = "Prefabs/Ui/Panels/WinPopup";
        public const string UIBalanceTextPath = "Prefabs/Ui/Balance/BalanceText";
        
        public const string ConfigPath = "StaticData/ConfigData/Config/GameConfig";
        public const string PositionCardsPath = "StaticData/Position/PositionCards";
        
        public const string SoundDataPath = "StaticData/Sound/SoundData";
        public const string ColorDataPath = "StaticData/ColorData/ColorData";
        public const string FortuneWheelPrefabsPath = "ReferenceData/FortuneWheelPrefabs";
    } 
}