using UnityEngine;

namespace FortuneWheel.Scripts.Data
{
    [CreateAssetMenu(fileName = "Fortune Wheel Prefabs", menuName = "Fortune Wheel/Fortune Wheel Prefabs")]
    public class FortuneWheelPrefabs : ScriptableObject
    {
        public GameObject PickerWheelPrefab;
        public GameObject RootUIPrefab;
        public GameObject SpinButtonPrefab;
        public GameObject ReturnButtonPrefab;
        public GameObject ResultPopupPrefab;
    }
} 