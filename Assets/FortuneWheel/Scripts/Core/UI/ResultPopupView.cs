using System;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace FortuneWheel.Scripts.Core.UI
{
    public class ResultPopupView : MonoBehaviour
    {
        public event Action OnAccept;

        [SerializeField] private CanvasGroup _canvasGroup;
        [SerializeField] private TextMeshProUGUI _resultText;
        [SerializeField] private Button _acceptButton;
        [SerializeField] private string _resultCurrency;

        private const float FadeDuration = 0.75f;
 
        private void Awake()
        {
            _canvasGroup.alpha = 0f;
            gameObject.SetActive(false);
            _acceptButton.onClick.AddListener(SendAccept);
        }

        private void OnDestroy() =>
            _acceptButton.onClick.RemoveListener(SendAccept);

        public void SetResultText(int result) =>
            _resultText.text = $"You win\n{result} {_resultCurrency}";
        
        public void Show()
        {
            gameObject.SetActive(true);
            _canvasGroup.DOFade(1, FadeDuration);
        }

        public void Hide()
        {
            _canvasGroup.DOFade(0, FadeDuration)
                .OnComplete(() => gameObject.SetActive(false));
        }

        private void SendAccept() => OnAccept?.Invoke();
    }
}