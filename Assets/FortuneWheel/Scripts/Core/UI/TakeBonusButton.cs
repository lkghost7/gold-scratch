﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace FortuneWheel.Scripts.Core.UI
{ 
    public class TakeBonusButton : MonoBehaviour
    { 
        public event Action OnReturnClicked;

        [SerializeField] private Button _returnButton;
         
        public void SetActiveOn() => gameObject.SetActive(true);
        public void Enable() => _returnButton.interactable = true;
        public void Disable() => _returnButton.interactable = false;
        
        private void Awake() => _returnButton.onClick.AddListener(SendReturnClick);
        private void OnDestroy() => _returnButton.onClick.RemoveListener(SendReturnClick);
        private void SendReturnClick() => OnReturnClicked?.Invoke();
    }
}