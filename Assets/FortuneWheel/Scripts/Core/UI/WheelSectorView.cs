using TMPro;
using UnityEngine;

namespace FortuneWheel.Scripts.Core.UI
{
    public class WheelSectorView : MonoBehaviour
    {
        [SerializeField] private TMP_Text _sectorValueText;

        public void SetValueText(int value) =>
            _sectorValueText.text = value.ToString();
    }
}