using System;
using UnityEngine;
using UnityEngine.UI;

namespace FortuneWheel.Scripts.Core.UI
{
    public class SpinButton : MonoBehaviour
    { 
        public event Action OnSpinClicked;

        [SerializeField] private Button _spinButton;
 
        private void Awake() => _spinButton.onClick.AddListener(SendSpinClick);

        private void OnDestroy() => _spinButton.onClick.RemoveListener(SendSpinClick);

        public void Enable() => _spinButton.interactable = true;
        
        public void Disable() => _spinButton.interactable = false;
        
        private void SendSpinClick() => OnSpinClicked?.Invoke();
    }
}