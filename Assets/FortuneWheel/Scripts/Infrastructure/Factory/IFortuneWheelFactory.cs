using EasyUI.PickerWheelUI;
using FortuneWheel.Scripts.Core.UI;
using UnityEngine;

namespace FortuneWheel.Scripts.Infrastructure.Factory
{
    public interface IFortuneWheelFactory
    {
        GameObject CreateRootUI();
        PickerWheel CreateFortuneWheel(Transform rootUI);
        SpinButton CreateSpinButton(Transform rootUI);
        TakeBonusButton CreateReturnGameButton(Transform rootUI);
        ResultPopupView CreateResultPopup(Transform rootUI);
    }
}