using CodeBase.Services.StaticData;
using EasyUI.PickerWheelUI;
using FortuneWheel.Scripts.Core.UI;
using FortuneWheel.Scripts.Data;
using UnityEngine;

namespace FortuneWheel.Scripts.Infrastructure.Factory
{
    public class FortuneWheelFactory : IFortuneWheelFactory
    {
        private readonly FortuneWheelPrefabs _prefabs;
        private IStaticDataService _staticDataService;

        public FortuneWheelFactory(IStaticDataService staticDataService)
        {
            _staticDataService = staticDataService;
            _prefabs = _staticDataService.FortuneWheelData;
        }

        public GameObject CreateRootUI() =>
            Object.Instantiate(_prefabs.RootUIPrefab);
 
        public PickerWheel CreateFortuneWheel(Transform rootUI)
        {
            PickerWheel pickerWheel = Object.Instantiate(_prefabs.PickerWheelPrefab, rootUI)
                .GetComponent<PickerWheel>();

            return pickerWheel;
        }

        public SpinButton CreateSpinButton(Transform rootUI)
        {
            return Object.Instantiate(_prefabs.SpinButtonPrefab, rootUI)
                .GetComponent<SpinButton>();
        }

        public TakeBonusButton CreateReturnGameButton(Transform rootUI)
        {
            return Object.Instantiate(_prefabs.ReturnButtonPrefab, rootUI)
                .GetComponent<TakeBonusButton>();
        }

        public ResultPopupView CreateResultPopup(Transform rootUI) =>
            Object.Instantiate(_prefabs.ResultPopupPrefab, rootUI)
                .GetComponent<ResultPopupView>();
    }
}