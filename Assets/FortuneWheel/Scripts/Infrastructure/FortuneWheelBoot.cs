using EasyUI.PickerWheelUI;
using FortuneWheel.Scripts.Core.PickerWheel;
using FortuneWheel.Scripts.Core.UI;
using FortuneWheel.Scripts.Data;
using FortuneWheel.Scripts.Infrastructure.Factory;
using UnityEngine;

namespace FortuneWheel.Scripts.Infrastructure
{
    public class FortuneWheelBoot : MonoBehaviour
    {
        [SerializeField] private FortuneWheelPrefabs _fortuneWheelPrefabs;

        private IFortuneWheelFactory _factory;
        private PickerWheel _pickerWheel;
        private SpinButton _spinButton;
        private ResultPopupView _resultPopup;
        
        private void Awake() => CreateGame();

        private void CreateGame()
        {
            Transform rootUI = _factory.CreateRootUI().transform;
            _pickerWheel = _factory.CreateFortuneWheel(rootUI);
            _spinButton = _factory.CreateSpinButton(rootUI);
            _resultPopup = _factory.CreateResultPopup(rootUI);
            
            _spinButton.OnSpinClicked += Spin;
            _resultPopup.OnAccept += OnPopupAccept;
            _pickerWheel.OnSpinEnd(FinishSpin);
        }

        private void Spin()
        {
            _pickerWheel.Spin();
            _spinButton.Disable();
        }

        private void OnPopupAccept()
        {
            _resultPopup.Hide();
            _spinButton.Enable();
        }

        private void FinishSpin(WheelPiece result)
        {
            _resultPopup.SetResultText(result.Amount);
            _resultPopup.Show();
        }
 
        private void OnDestroy()
        {
            _spinButton.OnSpinClicked -= Spin;
            _resultPopup.OnAccept -= OnPopupAccept;
        }
    }
}